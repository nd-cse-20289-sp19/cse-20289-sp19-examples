#!/usr/bin/env python3

import os
import re
import sys

import requests

# Globals

STATE  = 'Indiana'
CITY   = None
FORMAT = 'text'

# Functions

def usage(status=0):
    print('''Usage: {}
    -c      CITY    Which city to search
    -f      FORMAT  Which format (text, csv)
    -s      STATE   Which state to search (Indiana)'''.format(
        os.path.basename(sys.argv[0])
    ))
    sys.exit(status)

# Parse command line options

args = sys.argv[1:]
while args and args[0].startswith('-') and len(args[0]) > 1:
    arg = args.pop(0)
    if arg == '-c':
        CITY = args.pop(0)
    elif arg == '-s':
        STATE = args.pop(0)
    elif arg == '-h':
        usage(0)
    else:
        usage(1)

# Main execution

url      = 'https://www.zipcodestogo.com/{}/'.format(STATE)
#response = os.popen('curl -sL {}'.format(url)).read()
response = requests.get(url)
regex    = r'/([^/]+)/[A-Z]{2}/([0-9]{5})/">'

#for city, zipcode in re.findall(regex, response):
for city, zipcode in re.findall(regex, response.text):
    if CITY is None or city == CITY:
        print(zipcode)
