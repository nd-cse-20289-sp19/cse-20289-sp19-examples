/* heap.c */

#include <ctype.h>
#include <errno.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

char *str_alternate(const char *s) {
    /*
    char t[BUFSIZ];			// BUG: stack allocation
    strncpy(t, s, strlen(s) + 1);	// BUG: Off-by-1
    */
    char *t = malloc(strlen(s) + 1);	// BUG: Off-by-1
    strncpy(t, s, strlen(s) + 1);	// BUG: Off-by-1

    for (char *c = t; *c; c++) {
	*c = ((size_t)c % 2) ? toupper(*c) : tolower(*c);
    }

    // BUG: free?
    return t;
}

int main(int argc, char *argv[]) {
    /*
    int *h = malloc(INT_MAX*sizeof(int));

    // Doesn't fail on Linux
    if (h == NULL) {
    	fprintf(stderr, "malloc failed: %s\n", strerror(errno));
    	return EXIT_FAILURE;
    }

    // Show ps ux: virtual vs physical memory
    sleep(10);

    // Force physical allocation
    puts("away we go...");
    for (int i = 0; i < INT_MAX; i++) {
    	h[i] = i;
    	if (i % 10000 == 0) {
    	    usleep(1);
	}
    }
    */

    // Alternate arguments
    for (int i = 1; i < argc; i++) {
	char *t = str_alternate(argv[i]);
	puts(t);
	free(t);    // BUG: free
    }
    return EXIT_SUCCESS;
}
