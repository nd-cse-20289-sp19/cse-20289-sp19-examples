#!/bin/sh

# Download and install cmatrix

URL=https://www3.nd.edu/~pbui/teaching/cse.20289.sp19/static/tar/cmatrix-1.2a.tar.gz
PREFIX=${PREFIX:-/tmp/pub}

wget $URL
tar xvf $(basename $URL)

cd cmatrix-1.2a
./configure --prefix $PREFIX
make
make install
