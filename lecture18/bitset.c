/* bitset.c */

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

/* Constants */

const int DATA[] = {4, 6, 6, 3, 7, -1};

/* Functions */

int64_t bitset_insert(int64_t s, int value) {
    return s | (1<<value);
}

bool    bitset_search(int64_t s, int value) {
    return s & (1<<value);
}

/* Main Execution */

int main(int argc, char *argv[]) {
    int64_t s = 0;

    // Insert each item in DATA into set
    for (const int *p = DATA; *p >= 0; p++) {
    	s = bitset_insert(s, *p);
    }

    // Search for numbers 0 - 9 in set
    for (int i = 0; i < 10; i++) {
    	printf("%d ? %s\n", i, bitset_search(s, i) ? "YES" : "NO");
    }

    return EXIT_SUCCESS;
}
