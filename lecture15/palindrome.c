/* palindrome.c
 *
 * https://www.interviewcake.com/question/java/permutation-palindrome
 **/

#include <stdbool.h>
#include <stdio.h>
#include <string.h>

bool is_palindrome(const char *s) {
    const char *head = s;
    const char *tail = s + strlen(s) - 1;   // gdb: Off-by-one (- 1)

    while (head < tail && *head == *tail) { // valgrind: Uninitialized (head < tail)
    	head++;				    // gdb: Trace to verify
    	tail--;
    }

    return head >= tail;
}

bool is_palindrome_permutation(const char *s) {
    int counts[1<<8] = {0};		    // valgrind: Uninitialized (= {0})

    for (const char *c = s; *c; c++) {	    // gdb: Segmentation fault (*c = s)
    	counts[(int)*c]++;		    // Casting
    }

    int odds = 0;			    // valgrind: Uninitialized (= 0)
    for (int i = 0; i < 1<<8; i++) {
    	odds += counts[i] % 2;
    }

    return odds <= 1;
}

bool is_palindrome_permutation_bitset(const char *s) {
    int bset = 0;					// Bitset
    for (const char *c = s; *c; c++) {
    	bset ^= (1<<(*c - 'a'));			// Xor
    }

    int ones = 0;
    for (int i = 0; i < 8*sizeof(int); i++) {
    	ones += (bset & (1<<i) >> i);	    // gdb: trace with watchpoint (>> i)
    }

    return ones <= 1;
}

int main(int argc, char *argv[]) {
    char buffer[BUFSIZ];

    while (fgets(buffer, BUFSIZ, stdin)) {		// I/O
    	buffer[strlen(buffer) - 1] = 0;			// Chomp
    	puts(is_palindrome(buffer) ? "Yes" : "No");	// Ternary
    	//puts(is_palindrome_permutation(buffer) ? "Yes" : "No");
    	//puts(is_palindrome_permutation_bitset(buffer) ? "Yes" : "No");
    }

    return 0;
}
