/* copy.c */

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

int main(int argc, char *argv[]) {
    if (argc != 3) {
        fprintf(stderr, "Usage: %s [src] [dst]\n", argv[0]);
        return EXIT_FAILURE;
    }

    char *src = argv[1];
    char *dst = argv[2];

    /* Open file for reading */
    int rfd = open(src, O_RDONLY);
    if (rfd < 0) {
    	fprintf(stderr, "Unable to open %s: %s\n", src, strerror(errno));
    	return EXIT_FAILURE;
    }
    
    /* Open file for writing */
    int wfd = open(dst, O_CREAT|O_WRONLY, 0644);    // Add mode
    if (wfd < 0) {
    	fprintf(stderr, "Unable to open %s: %s\n", dst, strerror(errno));
    	return EXIT_FAILURE;
    }

    char buffer[BUFSIZ];
    int  nread;

    /* Read from source and write to target */
    while ((nread = read(rfd, buffer, BUFSIZ)) > 0) {
        int nwritten = 0;
        do {
            int wrote = write(wfd, buffer + nwritten, nread - nwritten); 
            if (wrote < 0) {
                fprintf(stderr, "Unable to write: %s\n", strerror(errno));
                return EXIT_FAILURE;
            }
            nwritten += wrote;
        } while (nwritten != nread);
    }
    
    close(rfd);
    close(wfd);
    return EXIT_SUCCESS;
}

/* vim: set sts=4 sw=4 ts=8 expandtab ft=c: */
